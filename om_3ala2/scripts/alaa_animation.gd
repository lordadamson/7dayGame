extends AnimationPlayer

var up
var c
var space
var tackling = false

signal tackling
signal tackling_finished

func _on_animation_changed(old_name, new_name):
	print("animation changed")
	if old_name == "tackling":
		emit_signal("tackling_finished")

func _ready():
	set_fixed_process(true)
	self.connect("animation_changed", self, "_on_animation_changed")

func _fixed_process(delta):
	up = Input.is_key_pressed(KEY_UP)
	c = Input.is_key_pressed(KEY_C)
	space = Input.is_key_pressed(KEY_SPACE)

	if up and not is_playing():
		play("run")
	elif not up:
		play("stop")
	if c and up and get_current_animation() != "tackling":
		play("tackling")
		tackling = true
		emit_signal("tackling")
	else:
		tackling = false
	if space and get_current_animation() != "jump":
		play("jump")