#extends KinematicBody
extends RigidBody

var position
var rotation

var right
var left
var up
var space



var sfx_player

var on_floor = true

var coin

func _on_tackling():
	print("tackling")

func _on_tacking_finished():
	print("tackling finished")

func _ready():
	set_process(true)
	rotation = get_rotation()
	position = get_translation()
	var animation_player = get_node("Spatial/AnimationPlayer") #animation player emits a signal when tackling so that we can change the size of the bounding box
	animation_player.connect("tackling", self, "_on_tackling")
	animation_player.connect("tackling_finished", self, "_on_tacking_finished")
	sfx_player = get_node("Spatial/alaa_sfx")
	

func _process(delta):
	
	
	right = Input.is_key_pressed(KEY_RIGHT)
	left = Input.is_key_pressed(KEY_LEFT)
	up = Input.is_key_pressed(KEY_UP)
	space = Input.is_key_pressed(KEY_SPACE)
	
	
	position = get_translation()
	
	if right:
		rotation.y -= 5*delta
		set_rotation(rotation)
	elif left:
		rotation.y += 5*delta
		set_rotation(rotation)
	if up:
		translate(Vector3(0, 0, -10*delta))
	if space and on_floor:
		translate(Vector3(0, 10*delta, 0))
		sfx_player.play("wingsFlapping")
