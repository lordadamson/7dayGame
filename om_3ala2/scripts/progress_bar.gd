extends ProgressBar

var coins
var max_coins

func _on_score_changed(score):
	set_val(score)

func _ready():
	main = get_node("/root/main")
	main.connect("score_changed", self, "_on_score_changed")
	max_coins = main.get_max_score()
	set_max(max_coins)
	set_val(0)